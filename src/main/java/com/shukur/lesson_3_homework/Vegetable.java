package com.shukur.lesson_3_homework;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class Vegetable implements Item{
    String name;

    @Override
    public String getName() {
        return name;
    }
}
