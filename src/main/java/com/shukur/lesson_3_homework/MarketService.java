package com.shukur.lesson_3_homework;

import java.util.HashMap;
import java.util.Map;

public class MarketService<T extends Item>{
    private Map<T, Integer> inventory = new HashMap<>();

    public void addItem(T item, int quantity) {
        inventory.put(item, inventory.getOrDefault(item, 0) + quantity);
    }

    public void removeItem(T item, int quantity) {
        int currentQuantity = inventory.getOrDefault(item, 0);
        if (currentQuantity > quantity) {
            inventory.put(item, currentQuantity - quantity);
        } else {
            inventory.remove(item);
        }
    }

    public void updateItem(T item, int quantity) {
        inventory.put(item, quantity);
    }

    public int getItemQuantity(T item) {
        return inventory.getOrDefault(item, 0);
    }

    public void printInventory() {
        for (Map.Entry<T, Integer> entry : inventory.entrySet()) {
            System.out.println(entry.getKey().getName() + ": " + entry.getValue());
        }
    }
}
