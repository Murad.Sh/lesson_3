package com.shukur.lesson_3_homework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson3HomeworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson3HomeworkApplication.class, args);
        MarketService<Fruit> fruits = new MarketService<>();
        Fruit apple = new Fruit("Apple");
        Fruit banana = new Fruit("Banana");
        Fruit orange = new Fruit("Orange");
        Fruit pear = new Fruit("Pear");

        fruits.addItem(apple, 10);
        fruits.addItem(banana, 20);
        fruits.addItem(orange, 30);
        fruits.printInventory();
        fruits.addItem(pear, 40);
        fruits.removeItem(orange, 15);
        fruits.updateItem(apple, 50);
        fruits.printInventory();
    }

}
